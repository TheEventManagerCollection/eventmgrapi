'''
Created on Nov 3, 2018

@author: jason0116
'''
from EventMgrAPI.Config import Config 
from requests import get
from requests import put
from requests import delete

import json

current_config = Config()

class Volunteer:
    
    def __init__(self, volunteer_id):
        self.id = volunteer_id
        
        data = get(current_config.http_uri + "/volunteers",
                   data={'volunteer_id': self.id}).json()
                   
        self.first_name = data['first_name']
        self.last_name = data['last_name']
        
	
        
    
    
