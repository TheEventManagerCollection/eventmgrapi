'''
Created on Nov 3, 2018

@author: jason0116
'''
from EventMgrAPI.Config import Config 
from requests import put

import json

current_config = Config()

class CreateUser():
    
    def __init__(self, first, last, interests):
        self.first = first
        self.last = last
        self.interests = interests
        
    def add(self):
        data = {}
        data['first_name'] = self.first
        data['last_name'] = self.last
        data['interests'] = json.dumps(self.interests)
        
        result = put(current_config.http_uri + "/user",
                   data=data).json()
        return result['success']           
        
