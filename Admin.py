'''
Created on Nov 3, 2018

@author: jason0116
'''
from EventMgrAPI.Config import Config 
from requests import get, put, delete
from requests.auth import HTTPBasicAuth

import hashlib
import json

current_config = Config()

class Admin():
    
	def __init__(self, admin_id):
		self.id = admin_id
        
		admin_data = get(current_config.http_uri + "/admin", data={'admin_id': self.id}).json()
                   
		self.first_name = admin_data['first_name']
		self.last_name = admin_data['last_name']
		self.email = admin_data['email']
		self.username = admin_data['username']
		self.hpassword = admin_data['hpassword']
	
	
	def check_password(self, password):
		return hashlib.sha256(password.encode('utf-8') + self.email.encode('utf-8')).hexdigest() == self.hpassword
	
########################################################################################################	

# add admin	
 
	def add_volunteer(self, first, last, admin_password):
		data = {}
		data['first_name'] = first
		data['last_name'] = last
                
		result = put(current_config.http_uri + "/volunteers", data=data, auth=HTTPBasicAuth(self.username, admin_password)).json()
		
		return result["success"]
    
	def add_admin(self, first, last, username, email, password, admin_password):
		# NOTE: admin_password is the password for the administrator who is adding another administrator
		
		data = {}
		
		data['first_name'] = first
		data['last_name'] = last
		data['email'] = email
		data['username'] = username
		data['password'] = password
		
		data['auth_username'] = self.username
		data['auth_password'] = admin_password
		
		result = put(current_config.http_uri + "/admin", data=data).json()
		
		return result['success']		
		
		
    
    
##################################################################################################################################################################    
    
# delete functions
    
	def delete_admin(self, admin, password):
		data = {}
		data['admin_id'] = admin.id
       
		result = delete(current_config.http_uri + "/admin",
					data=data, auth=HTTPBasicAuth(self.username, password)).json()
                   
       
		return result['success']
        
	def delete_user(self, user, password):
		data = {}
		data["user_id"] = user.id
		
		result = delete(current_config.http_uri + "/admin/delete_user", data=data, auth=HTTPBasicAuth(self.username, password)).json()
		
		return result['success']
	
	def delete_volunteer(self, volunteer, password):
			data = {}
			data['volunteer_id'] = volunteer.id
		   
			result = delete(current_config.http_uri + "/volunteers", data=data, auth=HTTPBasicAuth(self.username, password)).json()
					   
			return result['success']
			
			
##############################################################################################################

	def get_admins_list(self, admin_password):
		result = get(current_config.http_uri + "/admin/admin_list", auth=HTTPBasicAuth(self.username, admin_password)).json()
		return result
		
	def get_users_list(self, admin_password):
		result = get(current_config.http_uri + "/admin/user_list", auth=HTTPBasicAuth(self.username, admin_password)).json()
		return result
		
	def get_volunteers_list(self, admin_password):
		result = get(current_config.http_uri + "/admin/volunteer_list", auth=HTTPBasicAuth(self.username, admin_password)).json()
		print(result)
		
		return result
		
		
