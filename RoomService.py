'''
Created on Nov 3, 2018

@author: jason0116
'''
from EventMgrAPI.Config import Config 
from requests import get
from EventMgrAPI.User import User

import json

current_config = Config()

class RoomService:
    
	def __init__(self, roomservice_id):
		self.id = roomservice_id
        
		data = get(current_config.http_uri + "/room_service",
				data={'request_id': self.id}).json()
                   
		print(data)
		
		self.requester = User(data['requester_id'])
		self.description = data['description']
        
	@staticmethod
	def get_total_requests():
		return get(current_config.http_uri + "/room_service_count").json()
        

        
        
	
                  
        
       
