from requests import get
from EventMgrAPI.Config import Config

current_config = Config()

class EventInformation():
	def __init__(self):
		
		event_data = get(f"{current_config.http_uri}/event_info").json()
		
		self.event_name = event_data["event_name"]
		self.event_short_description = event_data["event_short_description"]
		self.event_long_description = event_data["event_long_description"]
