'''
Created on Nov 3, 2018

@author: jason0116
'''
class Config():
    
    http_hostname = "172.16.242.17"
    http_protocol = "http"
    http_port = 1915
    
    http_uri = f"{http_protocol}://{http_hostname}:{http_port}"
