from EventMgrAPI.Config import Config 
from requests import get, put, delete

import json

current_config = Config()

class User: # works at this stage
    
	def __init__(self, user_id):
		self.id = user_id
        
		data = get(current_config.http_uri + "/user",
					data={'user_id': self.id}).json()
                   
		self.first_name = data['first_name']
		self.last_name = data['last_name']
        
		self.interests = json.loads(data['interests'])
        
	def create_rs_request(self, description):
		"""Create a room service request"""
		
		data = {}
		
		data["user_id"] = self.id
		data["description"] = description
		
		result = put(current_config.http_uri + '/room_service', data=data).json()
		return result['success']
		
	def delete_rs_request(self, rs_request):
		"""Delete a room service request"""
		
		data = {}
		data["request_id"] = rs_request.id
		
		result = delete(current_config.http_uri + "/room_service", data=data).json()
		return result["success"]
		
	
		
		
		
		
		
		
